# Read a string:
# s = input()
# Print a value:
# print(s)
texto = input().split()
frecuencia = {}
for palabra in texto:
  if palabra not in frecuencia:
    frecuencia[palabra] = 0
  print(frecuencia[palabra], end=' ')
  frecuencia[palabra] += 1
