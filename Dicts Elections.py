# Read a string:
# s = input()
# Print a value:
# print(s)
n = int(input())
votos_total = {}
for i in range(n):
  candidato, num_votos = input().split()
  if candidato not in votos_total:
    votos_total[candidato] = 0
  votos_total[candidato] += int(num_votos)
for candidato in sorted(votos_total):
  print(candidato, votos_total[candidato])